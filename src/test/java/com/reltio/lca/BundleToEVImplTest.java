package com.reltio.lca;

import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.reltio.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import com.reltio.lca.relations.bundletoev.BundleToEVAfterDeleteImpl;
import com.reltio.lca.relations.bundletoev.BundleToEVAfterMergeImpl;
import com.reltio.lca.relations.bundletoev.BundleToEVAfterSaveImpl;
import com.reltio.lca.relations.bundletoev.BundleToEVAfterUnmergeImpl;
import com.reltio.lca.relations.bundletoev.BundleToEVAfterUpdateImpl;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.server.core.framework.ReltioAPI;
import com.reltio.lifecycle.server.core.services.LifeCycleHook;
import com.reltio.lifecycle.test.LifecycleExecutor;

public class BundleToEVImplTest {

	private final ObjectMapper mapper = new ObjectMapper();
	private IReltioAPI reltioAPI = getReltioAPI();

	@Test
    public void validateAfterSaveFunctionality() throws Exception {
		BundleToEVAfterSaveImpl handler = new BundleToEVAfterSaveImpl();
    	LifecycleExecutor executor = new LifecycleExecutor();
    	String input = Files.toString(new File(getClass().getResource("/BToEVBothMPMandVV.json").toURI()), Charset.defaultCharset());
    	executor.executeAction(handler, LifeCycleHook.afterSave, reltioAPI, input);
	}
    
    @Test
    public void validateAfterDeleteFunctionality() throws Exception {
    	BundleToEVAfterDeleteImpl handler = new BundleToEVAfterDeleteImpl();
    	LifecycleExecutor executor = new LifecycleExecutor();
    	String input = Files.toString(new File(getClass().getResource("/BToEVBothMPMandVV.json").toURI()), Charset.defaultCharset());
    	executor.executeAction(handler, LifeCycleHook.afterDelete, reltioAPI, input);
	}
    
    @Test
    public void validateAfterMergeFunctionality() throws Exception {
    	BundleToEVAfterMergeImpl handler = new BundleToEVAfterMergeImpl();
    	LifecycleExecutor executor = new LifecycleExecutor();
    	String input = Files.toString(new File(getClass().getResource("/BToEVBothMPMandVV.json").toURI()), Charset.defaultCharset());
    	executor.executeAction(handler, LifeCycleHook.afterMerge, reltioAPI, input);
	}
    
    @Test
    public void validateAfterUpdateFunctionality() throws Exception {
    	BundleToEVAfterUpdateImpl handler = new BundleToEVAfterUpdateImpl();
    	LifecycleExecutor executor = new LifecycleExecutor();
    	String input = Files.toString(new File(getClass().getResource("/BToEVBothMPMandVV.json").toURI()), Charset.defaultCharset());
    	executor.executeAction(handler, LifeCycleHook.afterUpdateBeforeCleanse, reltioAPI, input);
	}
    
    @Test
    public void validateAfterUnmergeFunctionality() throws Exception {
    	BundleToEVAfterUnmergeImpl handler = new BundleToEVAfterUnmergeImpl();
    	LifecycleExecutor executor = new LifecycleExecutor();
    	String input = Files.toString(new File(getClass().getResource("/BToEVBothMPMandVV.json").toURI()), Charset.defaultCharset());
    	executor.executeAction(handler, LifeCycleHook.afterUnmerge, reltioAPI, input);
	}
	
	private IReltioAPI getReltioAPI() {
		String token = null;
		try {
			token = getAuthorizationToken();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ReltioAPI("https://dev.reltio.com/reltio/", "C0Bxpp76rpACDYG", token);
	}

	private String getAuthorizationToken() throws Exception {
		String token = null;
		URL url = new URL(
				"https://auth.reltio.com/oauth/token?username=alagesan.govindan1&password=Omr7*oar&grant_type=password");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");
		conn.connect();

		InputStream is = conn.getInputStream();
		@SuppressWarnings("rawtypes")
		Map result = mapper.readValue(is, Map.class);
		is.close();
		token = (String) result.get("access_token");
		return token;
	}

}
