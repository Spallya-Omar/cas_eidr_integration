package com.reltio.lca;

import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.reltio.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import com.reltio.lca.relations.titletoev.TitleToEVAfterDeleteImpl;
import com.reltio.lca.relations.titletoev.TitleToEVAfterMergeImpl;
import com.reltio.lca.relations.titletoev.TitleToEVAfterSaveImpl;
import com.reltio.lca.relations.titletoev.TitleToEVAfterUnmergeImpl;
import com.reltio.lca.relations.titletoev.TitleToEVAfterUpdateImpl;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.server.core.framework.ReltioAPI;
import com.reltio.lifecycle.server.core.services.LifeCycleHook;
import com.reltio.lifecycle.test.LifecycleExecutor;

public class TitleToEVImplTest {

	private final ObjectMapper mapper = new ObjectMapper();
	private IReltioAPI reltioAPI = getReltioAPI();

	@Test
	public void validateAfterDeleteFunctionality() throws Exception {
		TitleToEVAfterDeleteImpl handler = new TitleToEVAfterDeleteImpl();
		LifecycleExecutor executor = new LifecycleExecutor();
		String input = Files.toString(new File(getClass().getResource("/TitleToEV.json").toURI()), Charset.defaultCharset());
		executor.executeAction(handler, LifeCycleHook.afterDelete, reltioAPI, input);
	}
	
	@Test
	public void validateAfterMergeFunctionality() throws Exception {
		TitleToEVAfterMergeImpl handler = new TitleToEVAfterMergeImpl();
		LifecycleExecutor executor = new LifecycleExecutor();
		String input = Files.toString(new File(getClass().getResource("/TitleToEV.json").toURI()), Charset.defaultCharset());
		executor.executeAction(handler, LifeCycleHook.afterMerge, reltioAPI, input);
	}
	
	@Test
	public void validateAfterSaveFunctionality() throws Exception {
		TitleToEVAfterSaveImpl handler = new TitleToEVAfterSaveImpl();
		LifecycleExecutor executor = new LifecycleExecutor();
		String input = Files.toString(new File(getClass().getResource("/TitleToEV.json").toURI()), Charset.defaultCharset());
		executor.executeAction(handler, LifeCycleHook.afterSave, reltioAPI, input);
	}

	@Test
    public void validateAfterUpdateFunctionality() throws Exception {
    	TitleToEVAfterUpdateImpl handler = new TitleToEVAfterUpdateImpl();
    	LifecycleExecutor executor = new LifecycleExecutor();
    	String input = Files.toString(new File(getClass().getResource("/TitleToEV.json").toURI()), Charset.defaultCharset());
    	executor.executeAction(handler, LifeCycleHook.afterUpdateBeforeCleanse, reltioAPI, input);
	}
    
    @Test
    public void validateAfterUnmergeFunctionality() throws Exception {
    	TitleToEVAfterUnmergeImpl handler = new TitleToEVAfterUnmergeImpl(	);
    	LifecycleExecutor executor = new LifecycleExecutor();
    	String input = Files.toString(new File(getClass().getResource("/TitleToEV.json").toURI()), Charset.defaultCharset());
    	executor.executeAction(handler, LifeCycleHook.afterUnmerge, reltioAPI, input);
	}
    
	private IReltioAPI getReltioAPI() {
		String token = null;
		try {
			token = getAuthorizationToken();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ReltioAPI("https://dev.reltio.com/reltio/", "C0Bxpp76rpACDYG", token);
	}

	private String getAuthorizationToken() throws Exception {
		String token = null;
		URL url = new URL(
				"https://auth.reltio.com/oauth/token?username=alagesan.govindan1&password=Omr7*oar&grant_type=password");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");
		conn.connect();

		InputStream is = conn.getInputStream();
		@SuppressWarnings("rawtypes")
		Map result = mapper.readValue(is, Map.class);
		is.close();
		token = (String) result.get("access_token");
		return token;
	}

}
