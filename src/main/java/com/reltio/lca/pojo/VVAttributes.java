package com.reltio.lca.pojo;

import java.util.List;

import com.fasterxml.reltio.jackson.annotation.JsonProperty;

public class VVAttributes {

	private Value EditEIDR;
	private List<Value> VideoVersionNumber;

	public Value getEditEIDR() {
		return EditEIDR;
	}
	
	@JsonProperty("EditEIDR")
	public void setEditEIDR(Value editEIDR) {
		EditEIDR = editEIDR;
	}

	public List<Value> getVideoVersionNumber() {
		return VideoVersionNumber;
	}
	
	@JsonProperty("VideoVersionNumber")
	public void setVideoVersionNumber(List<Value> videoVersionNumber) {
		VideoVersionNumber = videoVersionNumber;
	}

}
