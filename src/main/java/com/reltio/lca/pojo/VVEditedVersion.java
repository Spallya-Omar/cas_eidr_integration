package com.reltio.lca.pojo;

import java.util.List;

public class VVEditedVersion {

	private String type;
	private VVAttributes attributes;
	private List<Crosswalks> crosswalks;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public VVAttributes getAttributes() {
		return attributes;
	}

	public void setAttributes(VVAttributes attributes) {
		this.attributes = attributes;
	}

	public List<Crosswalks> getCrosswalks() {
		return crosswalks;
	}

	public void setCrosswalks(List<Crosswalks> crosswalks) {
		this.crosswalks = crosswalks;
	}

}
