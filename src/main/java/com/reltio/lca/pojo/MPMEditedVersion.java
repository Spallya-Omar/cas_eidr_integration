package com.reltio.lca.pojo;

import java.util.List;

public class MPMEditedVersion {

	private String type;
	private MPMAttributes attributes;
	private List<Crosswalks> crosswalks;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public MPMAttributes getAttributes() {
		return attributes;
	}

	public void setAttributes(MPMAttributes attributes) {
		this.attributes = attributes;
	}

	public List<Crosswalks> getCrosswalks() {
		return crosswalks;
	}

	public void setCrosswalks(List<Crosswalks> crosswalks) {
		this.crosswalks = crosswalks;
	}

}
