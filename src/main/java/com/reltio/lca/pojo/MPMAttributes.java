package com.reltio.lca.pojo;

import java.util.List;

import com.fasterxml.reltio.jackson.annotation.JsonProperty;

public class MPMAttributes {

	private Value EditEIDR;
	private List<Value> MPMNumber;

	public Value getEditEIDR() {
		return EditEIDR;
	}

	@JsonProperty("EditEIDR")
	public void setEditEIDR(Value editEIDR) {
		EditEIDR = editEIDR;
	}

	public List<Value> getMPMNumber() {
		return MPMNumber;
	}

	@JsonProperty("MPMNumber")
	public void setMPMNumber(List<Value> mpmNumber) {
		MPMNumber = mpmNumber;
	}

}
