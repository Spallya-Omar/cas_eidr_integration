package com.reltio.lca.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fasterxml.reltio.jackson.core.JsonParseException;
import com.fasterxml.reltio.jackson.core.JsonProcessingException;
import com.fasterxml.reltio.jackson.databind.JsonMappingException;
import com.fasterxml.reltio.jackson.databind.JsonNode;
import com.fasterxml.reltio.jackson.databind.ObjectMapper;
import com.reltio.lca.pojo.Crosswalks;
import com.reltio.lca.pojo.MPMAttributes;
import com.reltio.lca.pojo.MPMEditedVersion;
import com.reltio.lca.pojo.VVAttributes;
import com.reltio.lca.pojo.VVEditedVersion;
import com.reltio.lca.pojo.Value;
import com.reltio.lifecycle.framework.IReltioAPI;

/**
 * This class will be used by different LCA Hooks to post Video Version Numbers
 * or Mpm Numbers based on the requirement to the Edited Version
 * @author Spallya Omar
 */
public class LcaUtilities {

	private static final Logger logger = Logger.getLogger(LcaUtilities.class.getName());
	public static ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * @param reltioAPI
	 * @param objectMap
	 * @param type
	 */
	@SuppressWarnings("rawtypes")
	public static void getRelationsAndPostToEV(IReltioAPI reltioAPI, Map objectMap, String type) {
		logger.info("<<------ Starting getAndPostVVNumbersToEV ------>>");
		logger.info("Starting execution for TYPE: " + type);
		ArrayList<String> relationURIList;
		Map endObjectMap = (Map) objectMap.get(Constants.END_OBJECT_STRING);
		if (!endObjectMap.isEmpty()) {
			String endObjectURI = (String) endObjectMap.get(Constants.OBJECT_URI_STRING);
			relationURIList = getRelationURIList(reltioAPI, endObjectURI, type);
			if (!relationURIList.isEmpty()) {
				if (type.equals(Constants.PRODUCT) || type.equals(Constants.BUNDLE_VV)) {
					getVVNumbersAndPostToEV(reltioAPI, relationURIList, endObjectURI);
				} else if (type.equals(Constants.TITLE) || type.equals(Constants.BUNDLE_MPM)) {
					getMPMNumbersAndPostToEV(reltioAPI, relationURIList, endObjectURI);
				} else {
					throw new RuntimeException("Invalid relation type: " + type);
				}
			} else {
				if (type.equals(Constants.PRODUCT) || type.equals(Constants.BUNDLE_VV)) {
					postVVNumbersToEV(reltioAPI, null, getEditEIDR(reltioAPI, endObjectURI));
				} else if (type.equals(Constants.TITLE) || type.equals(Constants.BUNDLE_MPM)) {
					postMPMNumbersToEV(reltioAPI, null, getEditEIDR(reltioAPI, endObjectURI));
				} else {
					throw new RuntimeException("Invalid relation type: " + type);
				}
			}
		} else {
			throw new RuntimeException("Empty EndObject. Please contact administrator");
		}
		logger.info("<<------ Ending getAndPostVVNumbersToEV ------>>");
	}

	

	/**
	 * @param reltioAPI
	 * @param relationURIList
	 * @param endObjectURI
	 */
	public static void getMPMNumbersAndPostToEV(IReltioAPI reltioAPI, ArrayList<String> relationURIList, String endObjectURI) {
		ArrayList<String> mpmNumbersList;
		mpmNumbersList = getMPMNumbersList(reltioAPI, relationURIList);
		if (!mpmNumbersList.isEmpty()) {
			String editEIDR = getEditEIDR(reltioAPI, endObjectURI);
			if (!editEIDR.isEmpty()) {
				postMPMNumbersToEV(reltioAPI, mpmNumbersList, editEIDR);
			}
		} else {
			logger.info("MPM Number are not present for: " + endObjectURI);
		}
	}

	/**
	 * @param reltioAPI
	 * @param relationURIList
	 * @param endObjectURI
	 */
	public static void getVVNumbersAndPostToEV(IReltioAPI reltioAPI, ArrayList<String> relationURIList, String endObjectURI) {
		ArrayList<String> vvNumbersList;
		vvNumbersList = getVVNumbersList(reltioAPI, relationURIList);
		if (!vvNumbersList.isEmpty()) {
			String editEIDR = getEditEIDR(reltioAPI, endObjectURI);
			if (!editEIDR.isEmpty()) {
			}
			postVVNumbersToEV(reltioAPI, vvNumbersList, editEIDR);
		} else {
			logger.info("VideoVersion Number are not present for: " + endObjectURI);
		}
	}

	/**
	 * This method will fetch all the relationships uri(s) related to Edited Version
	 * @param reltioAPI
	 * @param endObjectURI
	 * @param type
	 * @return
	 */
	public static ArrayList<String> getRelationURIList(IReltioAPI reltioAPI, String endObjectURI, String type) {
		logger.info("<<------ Starting getRelationURIList ------>>");
		logger.info("Getting relation URI(s) from end object URI: " + endObjectURI);
		ArrayList<String> relationURIList = new ArrayList<String>();
		String connectionsString = ReltioUtilities.getConnectionsForEVRelations(reltioAPI, endObjectURI, type);
		if (!connectionsString.isEmpty()) {
			try {
				JsonNode jsonNode = objectMapper.readValue(connectionsString, JsonNode.class);
				JsonNode connectionNodes = jsonNode.get(0).get(Constants.CONNECTIONS_STRING);
				for (JsonNode connectionNode : connectionNodes) {
					String relationURI = String.valueOf(connectionNode.get(Constants.ENTITY_STRING).get(Constants.ENTITY_URI_STRING));
					relationURI = relationURI.substring(1, relationURI.length() - 1);
					logger.info("Relation URI for end object uri " + endObjectURI + " is: " + relationURI);
					relationURIList.add(relationURI);
				}
			} catch (JsonParseException e) {
				logger.error("Error while converting json string to json object. Please try again" + e);
				throw new RuntimeException("Error while converting json string to json object. Please try again" + e);
			} catch (JsonMappingException e) {
				logger.error("Error while converting json string to json object. Please try again" + e);
				throw new RuntimeException("Error while converting json string to json object. Please try again" + e);
			} catch (IOException e) {
				logger.error("Error while converting json string to json object. Please try again" + e);
				throw new RuntimeException("Error while converting json string to json object. Please try again" + e);
			} catch (Exception e) {
				logger.error("relation URI(s) is not present" + e);
			}
		} else {
			throw new RuntimeException("relation URI(s) are not present. Please check and try again later.");
		}
		logger.info("<<------ Ending getRelationURIList ------>>");
		return relationURIList;
	}

	/**
	 * This method will fetch all the Video Version Numbers related to Edited Version for which relationship is modified
	 * @param reltioAPI
	 * @param relationURIList
	 * @return
	 */
	public static ArrayList<String> getVVNumbersList(IReltioAPI reltioAPI, ArrayList<String> relationURIList) {
		logger.info("<<------ Starting getVVNumbersList ------>>");
		logger.info("Relation URI(s): " + relationURIList);
		ArrayList<String> vvNumbersList = new ArrayList<String>();
		try {
			for (String relationURI : relationURIList) {
				String response = ReltioUtilities.doGet(reltioAPI, relationURI + Constants.SELECT_VV_NUMBER, null);
				if (!response.isEmpty()) {
					JsonNode jsonNode = objectMapper.readValue(response, JsonNode.class);
					logger.info("Getting Video Version Number from relation URI: " + relationURI);
					String vvNumber = String.valueOf(jsonNode.get(Constants.ATTRIBUTES_STRING).get(Constants.VV_NUMBER_STRING).get(0).get(Constants.VALUE_STRING));
					vvNumber = vvNumber.substring(1, vvNumber.length() - 1);
					logger.info("Video Version Number from relation uri " + relationURI + " is: " + vvNumber);
					vvNumbersList.add(vvNumber);
				} else {
					throw new RuntimeException("Video Version Number(s) are not present. Please check and try again later.");
				}				
			}
		} catch (JsonParseException e) {
			logger.error("Error while converting json string to json object. Please try again" + e);
			throw new RuntimeException("Error while converting json string to json object. Please try again" + e);
		} catch (JsonMappingException e) {
			logger.error("Error while converting json string to json object. Please try again" + e);
			throw new RuntimeException("Error while converting json string to json object. Please try again" + e);
		} catch (IOException e) {
			logger.error("Error while converting json string to json object. Please try again" + e);
			throw new RuntimeException("Error while converting json string to json object. Please try again" + e);
		} catch (Exception e) {
			logger.error("Video Version Number is not present" + e);
		}
		logger.info("Final Mpm Number(s) List: " + vvNumbersList);
		logger.info("<<------ Ending getVVNumbersList ------>>");
		return vvNumbersList;
	}

	/**
	 * This method will get the EditEIDR of the particular Edited Version Record
	 * @param reltioAPI
	 * @param endObjectURI
	 * @return
	 */
	public static String getEditEIDR(IReltioAPI reltioAPI, String endObjectURI) {
		logger.info("<<------ Starting getEditEIDR ------>>");
		String editEDIRValue = null;
		String response = ReltioUtilities.doGet(reltioAPI, endObjectURI + Constants.SELECT_EDIT_EIDR, null);
		if (!response.isEmpty()) {
			try {
				JsonNode jsonNode = objectMapper.readValue(response, JsonNode.class);
				logger.info("Getting EditEIDR from end object URI: " + endObjectURI);
				editEDIRValue = String.valueOf(jsonNode.get(Constants.ATTRIBUTES_STRING).get(Constants.EDIT_EIDR_STRING).get(0).get(Constants.VALUE_STRING));
				editEDIRValue = editEDIRValue.substring(1, editEDIRValue.length() - 1);
				logger.info("EditEIDR from end object URI " + endObjectURI + " is: " + editEDIRValue);
			} catch (JsonParseException e) {
				logger.error("Error while converting json string to json object. Please try again" + e);
				throw new RuntimeException("Error while converting json string to json object. Please try again" + e);
			} catch (JsonMappingException e) {
				logger.error("Error while converting json string to json object. Please try again" + e);
				throw new RuntimeException("Error while converting json string to json object. Please try again" + e);
			} catch (IOException e) {
				logger.error("Error while converting json string to json object. Please try again" + e);
				throw new RuntimeException("Error while converting json string to json object. Please try again" + e);
			} catch (Exception e) {
				logger.error("EditEIDR is not present" + e);
			}
		} else {
			throw new RuntimeException("Invalid end object uri: " + endObjectURI + " Please check and try again later.");
		}
		logger.info("<<------ Ending getEditEIDR ------>>");
		return editEDIRValue;
	}

	/**
	 * This method will fetch all the Mpm Numbers related to Edited Version for which relationship is modified
	 * @param reltioAPI
	 * @param relationURIList
	 * @return mpmNumbersList
	 */
	public static ArrayList<String> getMPMNumbersList(IReltioAPI reltioAPI, ArrayList<String> relationURIList) {
		logger.info("<<------ Starting getMPMNumbersList ------>>");
		logger.info("Relation URI(s): " + relationURIList);
		ArrayList<String> mpmNumbersList = new ArrayList<String>();
		try {
			for (String relationURI : relationURIList) {
				String response = ReltioUtilities.doGet(reltioAPI, relationURI + Constants.SELECT_MPM_NUMBER, null);
				if (!response.isEmpty()) {
					JsonNode jsonNode = objectMapper.readValue(response, JsonNode.class);
					logger.info("Getting Mpm Number from relation URI: " + relationURI);
					String mpmNumber = String.valueOf(jsonNode.get(Constants.ATTRIBUTES_STRING).get(Constants.MPM_NUMBER_STRING).get(0).get(Constants.VALUE_STRING));
					mpmNumber = mpmNumber.substring(1, mpmNumber.length() - 1);
					logger.info("Mpm Number from relation uri " + relationURI + " is: " + mpmNumber);
					mpmNumbersList.add(mpmNumber);
				} else {
					throw new RuntimeException("MPMNumbers are not present. Please check and try again later.");
				}				
			}
		} catch (JsonParseException e) {
			logger.error("Error while converting json string to json object. Please try again" + e);
			throw new RuntimeException("Error while converting json string to json object. Please try again" + e);
		} catch (JsonMappingException e) {
			logger.error("Error while converting json string to json object. Please try again" + e);
			throw new RuntimeException("Error while converting json string to json object. Please try again" + e);
		} catch (IOException e) {
			logger.error("Error while converting json string to json object. Please try again" + e);
			throw new RuntimeException("Error while converting json string to json object. Please try again" + e);
		} catch (Exception e) {
			logger.error("Mpm Number is not present" + e);
		}
		logger.info("Final Mpm Number(s) List: " + mpmNumbersList);
		logger.info("<<------ Ending getMPMNumbersList ------>>");
		return mpmNumbersList;
	}
	
	/**
	 * This method will post the Video Version Numbers to Edited Version
	 * @param reltioAPI
	 * @param vvNumbersList
	 * @param editEIDR
	 */
	public static void postVVNumbersToEV(IReltioAPI reltioAPI, ArrayList<String> vvNumbersList, String editEIDR) {
		logger.info("<<------ Starting postVVNumbersToEV ------>>");
		logger.info("Posting Video Version Numbers" + vvNumbersList + " for EditEIDR: " + editEIDR);
		List<VVEditedVersion> editedVersionList = new ArrayList<VVEditedVersion>(1);
		VVEditedVersion editedVersion = new VVEditedVersion();
		VVAttributes attributes = new VVAttributes();
		Crosswalks crosswalks = new Crosswalks();
		List<Value> vvNumbersValueList = new ArrayList<Value>(1);
		List<Crosswalks> crosswalksList = new ArrayList<Crosswalks>(2);
		Value editEidrValue = new Value();
		crosswalks.setType(Constants.CROSSWALK_CUSTOM_TYPE);
		crosswalks.setValue(editEIDR);
		crosswalksList.add(crosswalks);
		editEidrValue.setValue(editEIDR);
		if(vvNumbersList == null) {
			Value vvNumberValue = new Value();
			vvNumberValue.setValue(null);
			vvNumbersValueList.add(vvNumberValue);
		} else {
			for (String vvNumber : vvNumbersList) {
				Value vvNumberValue = new Value();
				vvNumberValue.setValue(vvNumber);
				vvNumbersValueList.add(vvNumberValue);
			}
		}
		attributes.setEditEIDR(editEidrValue);
		attributes.setVideoVersionNumber(vvNumbersValueList);
		editedVersion.setType(Constants.EV_ENTITY_TYPE);
		editedVersion.setAttributes(attributes);
		editedVersion.setCrosswalks(crosswalksList);
		editedVersionList.add(editedVersion);
		String evRequestJsonString = null;
		try {
			evRequestJsonString = objectMapper.writeValueAsString(editedVersionList);
			logger.info("Posting JSON: " + evRequestJsonString);
			boolean response = ReltioUtilities.doPost(reltioAPI, Constants.PARTIAL_OVR_ENTITIES_URI, evRequestJsonString, null);
			if (response) {
				logger.info("Successfully posted the VideoVersionNumber for EditEIDR: " + editEIDR);
			} else {
				throw new RuntimeException("Error while posting the VideoVersionNumber for EditEIDR: " + editEIDR + " Please try again");
			}
		} catch (JsonProcessingException ex) {
			logger.error(ex);
			throw new RuntimeException("Error while converting json object to json string. Please try again" + ex);
		}
		logger.info("<<------ Ending postVVNumbersToEV ------>>");
	}

	/**
	 * This method will post the Mpm Numbers to Edited Version
	 * @param reltioAPI
	 * @param mpmNumbersList
	 * @param editEIDR
	 */
	public static void postMPMNumbersToEV(IReltioAPI reltioAPI, ArrayList<String> mpmNumbersList, String editEIDR) {
		logger.info("<<------ Starting postMPMNumbersToEV ------>>");
		logger.info("Posting MPM Numbers" + mpmNumbersList + " for EditEIDR: " + editEIDR);
		List<MPMEditedVersion> editedVersionList = new ArrayList<MPMEditedVersion>(1);
		MPMEditedVersion editedVersion = new MPMEditedVersion();
		MPMAttributes attributes = new MPMAttributes();
		Crosswalks crosswalks = new Crosswalks();
		List<Value> mpmNumbersValueList = new ArrayList<Value>(1);
		List<Crosswalks> crosswalksList = new ArrayList<Crosswalks>(2);
		Value editEidrValue = new Value();
		crosswalks.setType(Constants.CROSSWALK_CUSTOM_TYPE);
		crosswalks.setValue(editEIDR);
		crosswalksList.add(crosswalks);
		editEidrValue.setValue(editEIDR);
		if(mpmNumbersList == null) {
			Value mpmNumberValue = new Value();
			mpmNumberValue.setValue(null);
			mpmNumbersValueList.add(mpmNumberValue);
		} else {
			for (String mpmNumber : mpmNumbersList) {
				Value mpmNumberValue = new Value();
				mpmNumberValue.setValue(mpmNumber);
				mpmNumbersValueList.add(mpmNumberValue);
			}
		}
		attributes.setEditEIDR(editEidrValue);
		attributes.setMPMNumber(mpmNumbersValueList);
		editedVersion.setType(Constants.EV_ENTITY_TYPE);
		editedVersion.setAttributes(attributes);
		editedVersion.setCrosswalks(crosswalksList);
		editedVersionList.add(editedVersion);
		String evRequestJsonString = null;
		try {
			evRequestJsonString = objectMapper.writeValueAsString(editedVersionList);
			logger.info("Posting JSON: " + evRequestJsonString);
			boolean response = ReltioUtilities.doPost(reltioAPI, Constants.PARTIAL_OVR_ENTITIES_URI, evRequestJsonString, null);
			if (response) {
				logger.info("Successfully posted the MPMNumber for EditEIDR: " + editEIDR);
			} else {
				throw new RuntimeException("Error while posting the MPMNumber(s) for EditEIDR: " + editEIDR + " Please try again later");
			}
		} catch (JsonProcessingException ex) {
			logger.error(ex);
			throw new RuntimeException("Error while converting json object to json string. Please try again" + ex);
		}
		logger.info("<<------ Ending postMPMNumbersToEV ------>>");
	}
	
}
