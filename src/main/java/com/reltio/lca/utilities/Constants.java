package com.reltio.lca.utilities;

/**
 * This class contains all the constants that are used in different classes
 * @author Spallya Omar
 *
 */
public class Constants {

	public static String PRODUCT = "product";
	public static String TITLE = "title";
	public static String BUNDLE_VV = "bundleEV";
	public static String BUNDLE_MPM = "bundleMPM";

	public static String PRODUCT_TO_EV_RELATION_TYPE = "configuration/relationTypes/ProductToEditedVersions";
	public static String TITLE_TO_EV_RELATION_TYPE = "configuration/relationTypes/TitleToEditedVersions";
	public static String BUNDLE_TO_EV_RELATION_TYPE = "configuration/relationTypes/BundleToEditedVersions";
	public static String BUNDLE_TO_EV_MPM_RELATION_TYPE = "configuration/relationTypes/BundleToEditedVersionsMPM";
	public static String EV_ENTITY_TYPE = "configuration/entityTypes/EditedVersion";
	public static String CROSSWALK_CUSTOM_TYPE = "configuration/sources/Custom";

	public static String END_OBJECT_STRING = "endObject";
	public static String OBJECT_STRING = "object";
	public static String OBJECT_URI_STRING = "objectURI";
	public static String CONNECTIONS_STRING = "connections";
	public static String VV_NUMBER_STRING = "VideoVersionNumber";
	public static String ENTITY_STRING = "entity";
	public static String ENTITY_URI_STRING = "entityUri";
	public static String EDIT_EIDR_STRING = "EditEIDR";
	public static String MPM_NUMBER_STRING = "MPMNumber";
	public static String ATTRIBUTES_STRING = "attributes";
	public static String VALUE_STRING = "value";

	public static String ENTITIES_URI = "entities/";
	public static String PARTIAL_OVR_ENTITIES_URI = "entities?options=partialOverride,updateAttributeUpdateDates";
	public static String CONNECTIONS_URI = "/_connections";
	public static String SELECT_EDIT_EIDR = "?select=attributes.EditEIDR";
	public static String SELECT_VV_NUMBER = "?select=attributes.VideoVersionNumber";
	public static String SELECT_MPM_NUMBER = "?select=attributes.MPMNumber";
	public static String PRODUCT_TO_EV_CONNECTIONS_BODY = "[{\"entityTypes\":[\"configuration/entityTypes/Product\"],\"inRelations\":[\"configuration/relationTypes/ProductToEditedVersions\"],\"max\":100,\"offset\":0}]";
	public static String TITLE_TO_EV_CONNECTIONS_BODY = "[{\"entityTypes\":[\"configuration/entityTypes/Title\"],\"inRelations\":[\"configuration/relationTypes/TitleToEditedVersions\"],\"max\":100,\"offset\":0}]";
	public static String BUNDLE_TO_EV_CONNECTIONS_BODY = "[{\"entityTypes\": [\"configuration/entityTypes/Bundle\"],\"inRelations\": [\"configuration/relationTypes/BundleToEditedVersions\"],\"max\": 100,\"offset\": 0}]";
	public static String BUNDLE_TO_EV_MPM_CONNECTIONS_BODY = "[{\"entityTypes\": [\"configuration/entityTypes/Bundle\"],\"inRelations\": [\"configuration/relationTypes/BundleToEditedVersionsMPM\"],\"max\": 100,\"offset\": 0}]";
	
}
