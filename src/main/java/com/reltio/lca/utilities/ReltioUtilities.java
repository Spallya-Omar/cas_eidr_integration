package com.reltio.lca.utilities;

import java.util.Map;

import org.apache.log4j.Logger;

import com.reltio.lifecycle.framework.IReltioAPI;

public class ReltioUtilities {
	
	private final static Logger logger = Logger.getLogger(ReltioUtilities.class.getName());
	
	public static String doGet(IReltioAPI reltioAPI, String uri, Map<String, String> headers) {
		String responseString = null;
		String returnValue = null;
		int retryCount = 0;
		try {
			responseString = reltioAPI.get(uri, headers);
			if (responseString.contains("\"invalid_token\"")) {
				if (retryCount < 2) {
					retryCount++;
					doGet(reltioAPI, uri, headers);
				} else {
					retryCount = 0;
					throw new RuntimeException("Error during GET operation for " + uri + " Error: Invalid Token. Please contact admin.");
				}
			} else if (responseString.startsWith("[") || responseString.startsWith("{")) {
				returnValue = responseString;
			} else {
				throw new RuntimeException("Error during GET operation for " + uri + " Please try again");
			}
			return returnValue;
		} catch (Exception e) {
			logger.error(e);
			throw new RuntimeException("Error during GET operation for " + uri + " Please try again");
		}
	}
	
	public static boolean doPost(IReltioAPI reltioAPI, String uri, String body, Map<String, String> headers) {
		int retryCount = 0;
		String responseString = null;
		boolean returnValue = false;
		try {
			responseString = reltioAPI.post(uri, body, headers);
			if (responseString.contains("\"invalid_token\"")) {
				if (retryCount < 2) {
					retryCount++;
					doPost(reltioAPI, uri, body, headers);
				} else {
					retryCount = 0;
					throw new RuntimeException("Error during data " + uri + " post. Please contact administartor.");
				}
			} else if (responseString.startsWith("[") || responseString.startsWith("{")) {
				returnValue = true;
				
			} else {
				throw new RuntimeException("Error during data " + uri + " post. Please contact administartor.");
			}
			return returnValue;
		} catch (Exception ex) {
			logger.error(ex);
			throw new RuntimeException("Error during data " + uri + " post. Please contact administartor.");
		}
	}
	
	public static String doPostAndGetResponse(IReltioAPI reltioAPI, String uri, String body, Map<String, String> headers) {
		int retryCount = 0;
		String responseString = null;
		try {
			responseString = reltioAPI.post(uri, body, headers);
			if (responseString.contains("\"invalid_token\"")) {
				if (retryCount < 2) {
					retryCount++;
					doPostAndGetResponse(reltioAPI, uri, body, headers);
				} else {
					retryCount = 0;
					throw new RuntimeException("Error during data " + uri + " post. Please contact administartor.");
				}
			} else if (responseString.startsWith("[") || responseString.startsWith("{")) {
				return responseString;
				
			} else {
				throw new RuntimeException("Error during data " + uri + " post. Please contact administartor.");
			}
			return responseString;
		} catch (Exception ex) {
			logger.error(ex);
			throw new RuntimeException("Error during data " + uri + " post. Please contact administartor.");
		}
	}
	
	public static boolean doPut(IReltioAPI reltioAPI, String uri, String body, Map<String, String> headers) {
		int retryCount = 0;
		String responseString = null;
		boolean returnValue = false;
		try {
			responseString = reltioAPI.put(uri, body, headers);
			if (responseString.contains("\"invalid_token\"")) {
				if (retryCount < 2) {
					retryCount++;
					doPut(reltioAPI, uri, body, headers);
				} else {
					retryCount = 0;
					throw new RuntimeException("Error during data " + uri + " PUT. Please contact administartor.");
				}
			} else if (responseString.startsWith("[") || responseString.startsWith("{")) {
				returnValue = true;
				
			} else {
				throw new RuntimeException("Error during data " + uri + " PUT. Please contact administartor.");
			}
			return returnValue;
		} catch (Exception ex) {
			logger.error(ex);
			throw new RuntimeException("Error during data " + uri + " PUT. Please contact administartor.");
		}
	}

	public static String getConnectionsForEVRelations(IReltioAPI reltioAPI, String endObjectURI, String type) {
		if (type.equals(Constants.PRODUCT)) {
			return doPostAndGetResponse(reltioAPI, endObjectURI + Constants.CONNECTIONS_URI, Constants.PRODUCT_TO_EV_CONNECTIONS_BODY, null);
		} else if (type.equals(Constants.TITLE)) {
			return doPostAndGetResponse(reltioAPI, endObjectURI + Constants.CONNECTIONS_URI, Constants.TITLE_TO_EV_CONNECTIONS_BODY, null);
		}  else if (type.equals(Constants.BUNDLE_VV)) {
			return doPostAndGetResponse(reltioAPI, endObjectURI + Constants.CONNECTIONS_URI, Constants.BUNDLE_TO_EV_CONNECTIONS_BODY, null);
		}  else if (type.equals(Constants.BUNDLE_MPM)) {
			return doPostAndGetResponse(reltioAPI, endObjectURI + Constants.CONNECTIONS_URI, Constants.BUNDLE_TO_EV_MPM_CONNECTIONS_BODY, null);
		} else {
			return null;
		}
		
	}
	
}
