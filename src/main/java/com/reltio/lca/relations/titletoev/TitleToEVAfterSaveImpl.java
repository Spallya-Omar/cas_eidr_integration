package com.reltio.lca.relations.titletoev;

import java.util.Map;

import org.apache.log4j.Logger;

import com.reltio.lca.utilities.Constants;
import com.reltio.lca.utilities.LcaUtilities;
import com.reltio.lifecycle.framework.ILifeCycleObjectData;
import com.reltio.lifecycle.framework.IObject;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.framework.LifeCycleActionBase;

public class TitleToEVAfterSaveImpl extends LifeCycleActionBase {

	private final Logger logger = Logger.getLogger(TitleToEVAfterSaveImpl.class.getName());

	/* 
	 * This method overrides the reltio after save hook and triggers after the data saved in cassandra 
	 * */
	@SuppressWarnings("rawtypes")
	@Override
	public void afterSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
		logger.info("<<------ Starting TitleToEVAfterSaveImpl ------>>");
		IObject object = data.getObject();
		if (object.getType().equals(Constants.TITLE_TO_EV_RELATION_TYPE)) {
			Map objectMap = (Map) data.toMap().get(Constants.OBJECT_STRING);
			if (!objectMap.isEmpty()) {
				LcaUtilities.getRelationsAndPostToEV(reltioAPI, objectMap, Constants.TITLE);	
			} else {
				throw new RuntimeException("Empty object map for TitleToEVAfterSave. Please try again later.");
			}
			logger.info("<<------ Ending TitleToEVAfterSaveImpl ------>>");
		} else {
			throw new RuntimeException("Invalid implementation for relationType: " + object.getType());
		}
	}

}
