package com.reltio.lca.relations.titletoev;

import java.util.Map;

import org.apache.log4j.Logger;

import com.reltio.lca.utilities.Constants;
import com.reltio.lca.utilities.LcaUtilities;
import com.reltio.lifecycle.framework.ILifeCycleMergeData;
import com.reltio.lifecycle.framework.IObject;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.framework.LifeCycleActionBase;

public class TitleToEVAfterMergeImpl extends LifeCycleActionBase {
	
	private final Logger logger = Logger.getLogger(TitleToEVAfterMergeImpl.class.getName());

	/* 
	 * This method overrides the reltio's after merge hook and triggers after the data is merged in cassandra 
	 * */
	@SuppressWarnings("rawtypes")
	@Override
	public void afterMerge(IReltioAPI reltioAPI, ILifeCycleMergeData data) {
		logger.info("<<------ Starting TitleToEVAfterMergeImpl ------>>");
		IObject object = data.getWinner();
		if (object.getType().equals(Constants.TITLE_TO_EV_RELATION_TYPE)) {
			Map objectMap = (Map) data.toMap().get(Constants.OBJECT_STRING);
			if (!objectMap.isEmpty()) {
				LcaUtilities.getRelationsAndPostToEV(reltioAPI, objectMap, Constants.TITLE);	
			} else {
				throw new RuntimeException("Empty object map for TitleToEVAfterMerge. Please try again later.");
			}
			logger.info("<<------ Ending TitleToEVAfterMergeImpl ------>>");
		} else {
			throw new RuntimeException("Invalid implementation for relationType: " + object.getType());
		}
	}

}
