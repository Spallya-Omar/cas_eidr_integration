package com.reltio.lca.relations.bundletoev;

import java.util.Map;

import org.apache.log4j.Logger;

import com.reltio.lca.utilities.Constants;
import com.reltio.lca.utilities.LcaUtilities;
import com.reltio.lifecycle.framework.ILifeCycleMergeData;
import com.reltio.lifecycle.framework.IObject;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.framework.LifeCycleActionBase;

public class BundleToEVAfterMergeImpl extends LifeCycleActionBase {
	
	private final Logger logger = Logger.getLogger(BundleToEVAfterMergeImpl.class.getName());

	/* 
	 * This method overrides the reltio's after merge hook and triggers after the data is merged in cassandra 
	 * */
	@SuppressWarnings("rawtypes")
	@Override
	public void afterMerge(IReltioAPI reltioAPI, ILifeCycleMergeData data) {
		logger.info("<<------ Starting BundleToEVAfterMergeImpl ------>>");
		IObject object = data.getWinner();
		if (object.getType().equals(Constants.BUNDLE_TO_EV_RELATION_TYPE)) {
			Map objectMap = (Map) data.toMap().get(Constants.OBJECT_STRING);
			if (!objectMap.isEmpty()) {
				LcaUtilities.getRelationsAndPostToEV(reltioAPI, objectMap, Constants.BUNDLE_VV);	
			} else {
				throw new RuntimeException("Empty object map for BundleToEVAfterMerge. Please try again later.");
			}
			logger.info("<<------ Ending BundleToEVAfterMergeImpl ------>>");
		} else {
			throw new RuntimeException("Invalid implementation for relationType: " + object.getType());
		}
	}

}