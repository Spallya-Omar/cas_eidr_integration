package com.reltio.lca.relations.producttoev;

import java.util.Map;

import org.apache.log4j.Logger;

import com.reltio.lca.utilities.Constants;
import com.reltio.lca.utilities.LcaUtilities;
import com.reltio.lifecycle.framework.ILifeCycleUnmergeData;
import com.reltio.lifecycle.framework.IObject;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.framework.LifeCycleActionBase;

public class ProductToEVAfterUnmergeImpl extends LifeCycleActionBase {

	private final Logger logger = Logger.getLogger(ProductToEVAfterUnmergeImpl.class.getName());

	/* 
	 * This method overrides the reltio's after unmerge hook and triggers after the data is unmerged in cassandra 
	 * */
	@SuppressWarnings("rawtypes")
	@Override
	public void afterUnmerge(IReltioAPI reltioAPI, ILifeCycleUnmergeData data) {
		logger.info("<<------ Starting ProductToEVAfterUnmergeImpl ------>>");
		IObject object = data.getObject();
		if (object.getType().equals(Constants.PRODUCT_TO_EV_RELATION_TYPE)) {
			Map objectMap = (Map) data.toMap().get(Constants.OBJECT_STRING);
			if (!objectMap.isEmpty()) {
				LcaUtilities.getRelationsAndPostToEV(reltioAPI, objectMap, Constants.PRODUCT);
			} else {
				throw new RuntimeException("Empty object map for ProductToEVAfterUnmergeImpl. Please try again later.");
			}
			logger.info("<<------ Ending ProductToEVAfterUnmergeImpl ------>>");
		} else {
			throw new RuntimeException("Invalid implementation for relationType: " + object.getType());
		}
	}
}
