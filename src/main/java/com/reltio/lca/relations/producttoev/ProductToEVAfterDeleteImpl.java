package com.reltio.lca.relations.producttoev;

import java.util.Map;

import org.apache.log4j.Logger;

import com.reltio.lca.utilities.Constants;
import com.reltio.lca.utilities.LcaUtilities;
import com.reltio.lifecycle.framework.ILifeCycleObjectData;
import com.reltio.lifecycle.framework.IObject;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.framework.LifeCycleActionBase;

public class ProductToEVAfterDeleteImpl extends LifeCycleActionBase {
	
	private final Logger logger = Logger.getLogger(ProductToEVAfterDeleteImpl.class.getName());

	/* 
	 * This method overrides the reltio's after delete hook and triggers after the data deleted from cassandra 
	 * */
	@SuppressWarnings("rawtypes")
	@Override
	public void afterDelete(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
		logger.info("<<------ Starting ProductToEVAfterDeleteImpl ------>>");
		IObject object = data.getObject();
		if (object.getType().equals(Constants.PRODUCT_TO_EV_RELATION_TYPE)) {
			Map objectMap = (Map) data.toMap().get(Constants.OBJECT_STRING);
			if (!objectMap.isEmpty()) {
				LcaUtilities.getRelationsAndPostToEV(reltioAPI, objectMap, Constants.PRODUCT);	
			} else {
				throw new RuntimeException("Empty object map for ProductToEVAfterDelete. Please try again later.");
			}
			logger.info("<<------ Ending ProductToEVAfterDeleteImpl ------>>");
		} else {
			throw new RuntimeException("Invalid implementation for relationType: " + object.getType());
		}
	}

}
