package com.reltio.lca.relations.bundletoevmpm;

import java.util.Map;

import org.apache.log4j.Logger;

import com.reltio.lca.utilities.Constants;
import com.reltio.lca.utilities.LcaUtilities;
import com.reltio.lifecycle.framework.ILifeCycleUnmergeData;
import com.reltio.lifecycle.framework.IObject;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.framework.LifeCycleActionBase;

public class BundleToEVMPMAfterUnmergeImpl extends LifeCycleActionBase {

	private final Logger logger = Logger.getLogger(BundleToEVMPMAfterUnmergeImpl.class.getName());

	/* 
	 * This method overrides the reltio's after unmerge hook and triggers after the data is unmerged in cassandra 
	 * */
	@SuppressWarnings("rawtypes")
	@Override
	public void afterUnmerge(IReltioAPI reltioAPI, ILifeCycleUnmergeData data) {
		logger.info("<<------ Starting BundleToEVMPMAfterUnmergeImpl ------>>");
		IObject object = data.getObject();
		if (object.getType().equals(Constants.BUNDLE_TO_EV_MPM_RELATION_TYPE)) {
			Map objectMap = (Map) data.toMap().get(Constants.OBJECT_STRING);
			if (!objectMap.isEmpty()) {
				LcaUtilities.getRelationsAndPostToEV(reltioAPI, objectMap, Constants.BUNDLE_MPM);
			} else {
				throw new RuntimeException("Empty object map for BundleToEVMPMAfterUnmerge. Please try again later.");
			}
			logger.info("<<------ Ending BundleToEVMPMAfterUnmergeImpl ------>>");
		} else {
			throw new RuntimeException("Invalid implementation for relationType: " + object.getType());
		}
	}
}
